[![pipeline status](https://gitlab.com/nicolalandro/simple-pink-noise/badges/master/pipeline.svg)](https://gitlab.com/nicolalandro/simple-pink-noise/-/commits/master)
[![Licence: TODO](https://img.shields.io/badge/licence-GPL--3.0-lightgrey)](LICENSE)

[![Licence: TODO](https://img.shields.io/badge/package-LV2-green)](https://gitlab.com/nicolalandro/simple-pink-noise/-/packages)

# Pink Noise
The [pink noise technique](https://www.youtube.com/watch?v=_VofOoDTdD8) is very usefull for mixing, so we need a plugin to do it quickly. 
A vst for this already exist ([official](http://www.credland.net/pink/), [unofficial](https://freevstplugins.net/pink/)), but it is not open source and it is not Linux Native. 

![UI](img/UI.png)

## Install
The continous deployment release can be seen at [link](https://gitlab.com/nicolalandro/simple-pink-noise/-/packages). 

```
# wget -O simple-pink.tar.gz https://gitlab.com/nicolalandro/simple-pink-noise/-/package_files/6347440/download
tar -xvzf simple-pink.tar.gz

cd lv2_no_gui
# or cd lv_qt if you want qt5 version 

sudo cp -fr simple_pink.lv2 /usr/lib/lv2/
# or: sudo cp -fr simple_pink.lv2 /usr/local/lib/lv2/simple_pink.lv2
```

# DEV
* Install [faust](https://github.com/grame-cncm/faust):
```
# instlal requiremenst
git clone https://github.com/grame-cncm/faust.git
cd faust
git submodule update --init
make
sudo make install
```

## Compile excutable Jack with Qt gui
```
cd faust
faust2jaqt simple_pink.dsp 
```

## Compile lv2 no gui
```
cd faust
faust2lv2 simple_pink.dsp 
```

## Compile lv2 with qt5
```
cd faust
faust2lv2 -qt5 simple_pink.dsp 
```
## Using Docker
Install [Docker](https://docs.docker.com/get-docker/).

```
./build_with_docker.sh

# change property
sudo chmod -R 777 simple_pink simple_pink.lv2/ 

# clean 
sudo rm -rf simple_pink simple_pink.lv2/ 
```

# Study Reference
This study reference contains all my study to make this plugin, so I think that a contributor can be interested in.

* [Faust web ide](https://faustide.grame.fr/)
* [Faust manual](https://faustcloud.grame.fr/doc/manual/index.html)

