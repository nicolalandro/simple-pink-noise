declare name 		"Simple Pink";
declare version 	"0.1";
declare author 		"Nicola Landro";
declare license 	"GPL-3.0";

import("stdfaust.lib");

volume = vslider("Volume[style:knob]", .5, 0, 1, 0.1);
noise = vslider("Noise[style:knob]", 1, 0, 2, 0.1);
intensity = hgroup("Noise Control",noise*volume);
on = checkbox("On/Off");

freq = no.pink_noise;

process = freq * on * intensity;

