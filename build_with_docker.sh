#!/bin/bash
docker build -t registry.gitlab.com/nicolalandro/simple-pink-noise ./Docker/
docker run -it --rm -v $(pwd):/code registry.gitlab.com/nicolalandro/simple-pink-noise bash -c "faust2lv2 -qt5 -keep /code/simple_pink.dsp"

# change property
# sudo chmod -R 777 simple_pink simple_pink.lv2/ 

# clean 
# sudo rm -rf simple_pink simple_pink.lv2/ 