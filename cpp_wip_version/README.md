# Pink Noise
The [pink noise technique](https://www.youtube.com/watch?v=_VofOoDTdD8) is very usefull for mixing, so we need a plugin to do it quickly. 
A vst for this already exist ([official](http://www.credland.net/pink/), [unofficial](https://freevstplugins.net/pink/)), but it is not open source and it is not Linux Native. 

WIP

![UI](img/inkscape_UI.png)

## Pre requirements
```
sudo apt install lv2-c++-tools
```

## Compile & Install

```
make
sudo make install
```

## Clean

```
make clean
```

## Uninstall

```
sudo make uninstall
```

# Study Reference
This study reference contains all my study to make this plugin, so I think that a contributor can be interested in.

* [LV2 official page](https://lv2plug.in/)
* [LV2 programming for the complete idiot](http://ll-plugins.nongnu.org/lv2pftci/)
* [Making an LV2 plugin GUI (yes, in Inkscape)](http://mountainbikesandtrombones.blogspot.com/2014/08/making-lv2-plugin-gui-yes-in-inkscape.html)
* [Knob Scale Generator - inkscape plugin](https://inkscape.org/~sincoon/%E2%98%85knob-scale-generator)
* [wiki PinkNoise](https://en.wikipedia.org/wiki/Pink_noise)


