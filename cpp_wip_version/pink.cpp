#include <lv2plugin.hpp>


using namespace LV2;


class Pink : public Plugin<Pink> {
public:
  
  Pink(double rate)
    : Plugin<Pink>(3) {
    
  }
  
  void run(uint32_t nframes) {
    for (uint32_t i = 0; i < nframes; ++i)
      p(2)[i] = 0;
  }

};


static int _ = Pink::register_class("http://ll-plugins.nongnu.org/lv2/lv2pftci/simplepink");
